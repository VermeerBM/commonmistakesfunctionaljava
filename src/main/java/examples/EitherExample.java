package examples;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


import types.Either;

public class EitherExample {

    private Either<Exception, Integer> canGoWrong(Integer input) {
        if (input > 10) {
            return Either.Left(new RuntimeException(input + " too big"));
        }
        return Either.Right(input);

    }


    private void execute() {
        List<Either<Exception, Integer>> results = IntStream.iterate(0, i -> i+1)
            .limit(20)
            .mapToObj(this::canGoWrong)
            .collect(Collectors.toList());

        results.forEach(System.out::println);

//        results.stream()
//            .flatMap(either -> either.mapRight(i -> i * 5).stream())
//            .forEach(System.out::println);


//
//        results.stream()
//            .filter(Either::isLeft)
//            .map(either -> either.getLeft().get())
//            .forEach(System.out::println);
    }





    public static void main(String[] args) {
        EitherExample ee = new EitherExample();
        ee.execute();
    }
}
