package examples;

import static java.lang.Math.random;
import static java.lang.Math.round;

import java.util.List;
import java.util.stream.Collectors;

import exception.MyException;
import io.vavr.CheckedFunction1;
import io.vavr.control.Try;

public class VavrExceptionHandling {

    List<String> teams = List.of("alpha", "bravo", "charlie", "delta", "echo", "foxtrot", "golf");

    private String mayThrowException(String a) throws MyException {
        int randomValue = (int) round(random() * 10);
        if (randomValue % 2 == 0) {
            throw new MyException("AAAAAAH");
        }

        return a;
    }



    private void execute() {
        List<Try<String>> output = teams.stream()
            .map(CheckedFunction1.liftTry(this::mayThrowException))
            .collect(Collectors.toList());

        output.forEach(System.out::println);

//        output.stream()
//            .filter(tryStr -> tryStr.isFailure())
//            .forEach(fail -> fail.getCause().printStackTrace());
    }


    public static void main(String[] args) {
        VavrExceptionHandling program = new VavrExceptionHandling();
        program.execute();
    }


}
