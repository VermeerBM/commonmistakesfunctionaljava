package examples;

import java.util.function.Function;

import types.Beer;

public class TooMuchLambda {

    private void execute() {
        Beer grolsch =  new Beer("Grolsch", 4.3);
        String result = handleBeer(grolsch, beer -> {
                String name;
                if (beer.getName().contains(" ")) {
                    name = beer.getName().replace(" ", "");
                } else {
                    name = beer.getName();
                }

                try {
                    name += Integer.parseInt(beer.getAlcohol().toString());
                } catch (NumberFormatException nfe) {
                    name += beer.getAlcohol();
                }

                return name;
            });
        System.out.println(result);

    }

    private String handleBeer(Beer beer, Function<Beer, String> func) {
        System.out.println("handling beer " + beer.getName());
        return func.apply(beer);
    }


    public static void main(String[] args) {
        TooMuchLambda tml = new TooMuchLambda();
        tml.execute();
    }

}
