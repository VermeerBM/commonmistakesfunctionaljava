package examples;

import java.util.List;
import java.util.stream.Collectors;

import types.Beer;

public class ConsumingStream {

    List<Beer> beers = List.of(new Beer("Heineken", 5.2), new Beer("Delirium Tremens", 9.0), new Beer("Amstel", 5.1));

    private void execute() {
        beers.stream()
            .limit(10)
            .map(beer -> beer.getAlcohol())
            .peek(i -> {
                if (i>7.0)
                    throw new RuntimeException();
            });
    }






    public static void main(String[] args) {
        ConsumingStream cs = new ConsumingStream();
        cs.execute();
    }
}
