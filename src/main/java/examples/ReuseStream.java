package examples;

import java.util.List;
import java.util.stream.Stream;

import types.Beer;

public class ReuseStream {

    private final List<Beer> beers = List.of(new Beer("Heineken", 5.2), new Beer("Amstel", 5.1));

    private void execute() {
        Stream<Beer> beerStream = beers.stream();
    }

//    private Stream<Beer> getBeerStream() {
//        return beers.stream();
//    }


    public static void main(String[] args) {
        ReuseStream rus = new ReuseStream();
        rus.execute();
    }

}
