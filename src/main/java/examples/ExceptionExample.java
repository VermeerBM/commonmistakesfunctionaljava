package examples;

import java.util.List;

import exception.MyException;
import types.Beer;


public class ExceptionExample {

    public Beer doSomething(Beer beer) throws MyException {
        if (Math.random() * 10 > 5) throw new MyException("aaaaah");
        return beer;
    }

    private final List<Beer> beerLib = List.of(new Beer("Heineken", 5.2)
        , new Beer("Amstel", 5.1)
        , new Beer("Grolsch", 4.8)
        );

    public void execute() {
        beerLib.stream()
//            .map(beer -> doSomething(beer))
            .forEach(System.out::println);
    }

    public static void main(String[] args) {
        ExceptionExample ee = new ExceptionExample();
        ee.execute();
    }

}
