package types;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class Beer {
    String name;
    Double alcohol;


//    public Beer setName(String name) {
//        this.name = name;
//        return this;
//    }



}
