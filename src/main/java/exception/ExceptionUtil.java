package exception;

import java.util.function.Function;

public class ExceptionUtil {

    public static <T,R> Function<T,R> wrap(CheckedFunction<T,R> function) {
        return t -> {
            try {
                return function.apply(t);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        };
    }
}
